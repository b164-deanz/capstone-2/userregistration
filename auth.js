const jwt = require("jsonwebtoken")
const secret = "PapasaAkoSaCapstone2";


module.exports.createAccessToken = (user) => {
	const userData = {
		id: userData._id,
		email: userData.email,
		isAdmin: userData.isAdmin
	}
	return jwt.sign(userData, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		console.log(token)
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(Err){
				return res.send({auth: "failed"})
			}else{
				next()
			}
		})
	}else{
		return res.send({ auth: "token undefined"})
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null
			} else {
				return jwt.decode(token, { complete:true }).payload;
			}
		})
	}else{
		return null
	}
}