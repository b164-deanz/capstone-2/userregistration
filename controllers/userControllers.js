const User = require("../models/User")


const bcrypt = require("bcrypt")

const auth = require("../auth")

module.exports.registerUser = (reqBody) => {
	let registerUsers = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 8)
	})
	return registerUsers.save().then((register, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	})
}

module.exports.userLogin = (reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { YourToken: auth.createAccessToken(result.toObject())}
			}else{
				return false
			}
		}
	})
}