const express = require("express")
const router = express.Router()

const UserController = require("../controllers/userControllers")

const auth = require("../auth")

router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result))
})


router.post("/userLogin", (req, res) => {
	UserController.userLogin(req.body).then(result => res.send(result))
})



module.exports = router;